/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bestza012.swingox;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author WIN10
 */
public class WriteObject {
    public static void main(String[] args) {
        File file = null;
        FileOutputStream fos = null; //if not declare this block it will can't close;
        ObjectOutputStream oos = null;
        User x = new User('x');
        User o = new User('o');
        x.win();
        x.lose();
        x.draw();
        o.win();
        o.lose();
        o.draw();
        System.out.println(x);
        System.out.println(o);
        try {
            file = new File("ox.bin");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(x);
            oos.writeObject(o);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(WriteObject.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WriteObject.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                oos.close();
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(WriteObject.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }
}
