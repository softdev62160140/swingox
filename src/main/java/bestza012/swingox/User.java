/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bestza012.swingox;

import java.io.Serializable;

public class User implements Serializable {

    private char name;
    private int win = 0;
    private int lose = 0;
    private int draw = 0;

    public User(char name) {
        this.name = name;
    }

    public char getName() {
        return name;
    }

    public int getWin() {
        return win;
    }

    public int win() {
        return win++;
    }

    public int getLose() {
        return lose;
    }

    public int lose() {
        return lose++;
    }

    public int getDraw() {
        return draw;
    }

    public int draw() {
        return draw++;
    }

    public void setName(char name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "User{" + "name=" + name + ", win=" + win + ", lose=" + lose + ", draw=" + draw + '}';
    }
}
